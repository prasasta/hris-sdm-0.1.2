<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211014015834 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE t_usulan_hari_libur (id UUID NOT NULL, nomor_surat_dasar VARCHAR(120) NOT NULL, tanggal_surat_dasar DATE NOT NULL, keterangan_surat_dasar TEXT DEFAULT NULL, id_pembuat UUID NOT NULL, role_pembuat VARCHAR(255) NOT NULL, tanggal_buat DATE NOT NULL, id_pengupdate UUID DEFAULT NULL, role_pengupdate VARCHAR(255) DEFAULT NULL, tanggal_update DATE DEFAULT NULL, status INT NOT NULL, id_atasan_approve UUID DEFAULT NULL, tanggal_atasan_approve DATE DEFAULT NULL, id_kakap_approve UUID DEFAULT NULL, tanggal_kakap_approve DATE DEFAULT NULL, id_upk_pusat_approve UUID DEFAULT NULL, tanggal_upk_pusat_approve DATE DEFAULT NULL, nomor_ticket INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_usulan_hari_libur ON t_usulan_hari_libur (id, nomor_surat_dasar, status, nomor_ticket)');
        $this->addSql('CREATE INDEX idx_usulan_hari_libur_ids ON t_usulan_hari_libur (id, id_pembuat, id_pengupdate, id_atasan_approve, id_kakap_approve, id_upk_pusat_approve)');
        $this->addSql('COMMENT ON COLUMN t_usulan_hari_libur.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_usulan_hari_libur.id_pembuat IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_usulan_hari_libur.id_pengupdate IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_usulan_hari_libur.id_atasan_approve IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_usulan_hari_libur.id_kakap_approve IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_usulan_hari_libur.id_upk_pusat_approve IS \'(DC2Type:uuid)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE t_usulan_hari_libur');
    }
}

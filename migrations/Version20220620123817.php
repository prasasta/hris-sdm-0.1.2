<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220620123817 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE t_permohonan_cuti_tambahan (id UUID NOT NULL, permohonan_cuti_id UUID NOT NULL, jenis_id INT NOT NULL, tanggal_mulai DATE NOT NULL, tanggal_selesai DATE NOT NULL, lama_cuti_tambahan DOUBLE PRECISION NOT NULL, alasan_cuti_tambahan TEXT NOT NULL, status INT NOT NULL, approval_atasan_langsung INT DEFAULT NULL, approval_atasan_berwenang INT DEFAULT NULL, pegawai_id UUID NOT NULL, atasan_langsung UUID DEFAULT NULL, nip_atasan_langsung VARCHAR(18) DEFAULT NULL, nama_atasan_langsung VARCHAR(128) DEFAULT NULL, atasan_berwenang UUID DEFAULT NULL, nip_atasan_berwenang VARCHAR(18) DEFAULT NULL, nama_atasan_berwenang VARCHAR(128) DEFAULT NULL, tanggal_persetujuan DATE DEFAULT NULL, nama_setuju UUID DEFAULT NULL, alasan_pembatalan TEXT DEFAULT NULL, batal INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8B50402FD5CC105 ON t_permohonan_cuti_tambahan (permohonan_cuti_id)');
        $this->addSql('CREATE INDEX IDX_8B50402FBBD1681E ON t_permohonan_cuti_tambahan (jenis_id)');
        $this->addSql('CREATE INDEX idx_permohonan_cuti_tambahan ON t_permohonan_cuti_tambahan (id, permohonan_cuti_id, jenis_id, tanggal_mulai, tanggal_selesai)');
        $this->addSql('COMMENT ON COLUMN t_permohonan_cuti_tambahan.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_permohonan_cuti_tambahan.permohonan_cuti_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_permohonan_cuti_tambahan.pegawai_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_permohonan_cuti_tambahan.atasan_langsung IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_permohonan_cuti_tambahan.atasan_berwenang IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_permohonan_cuti_tambahan.nama_setuju IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE t_permohonan_cuti_tambahan ADD CONSTRAINT FK_8B50402FD5CC105 FOREIGN KEY (permohonan_cuti_id) REFERENCES t_permohonan_cuti (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE t_permohonan_cuti_tambahan ADD CONSTRAINT FK_8B50402FBBD1681E FOREIGN KEY (jenis_id) REFERENCES t_jns_cuti_tambahan (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE t_permohonan_cuti_tambahan');
    }
}

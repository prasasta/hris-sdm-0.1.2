<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211111041053 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE t_usulan_hari_libur ALTER keterangan_surat_dasar TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE t_usulan_hari_libur ALTER keterangan_surat_dasar DROP DEFAULT');
        $this->addSql('ALTER TABLE t_usulan_hari_libur ALTER nomor_ticket TYPE VARCHAR(50)');
        $this->addSql('ALTER TABLE t_usulan_hari_libur ALTER nomor_ticket DROP DEFAULT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE t_usulan_hari_libur ALTER keterangan_surat_dasar TYPE TEXT');
        $this->addSql('ALTER TABLE t_usulan_hari_libur ALTER keterangan_surat_dasar DROP DEFAULT');
        $this->addSql('ALTER TABLE t_usulan_hari_libur ALTER nomor_ticket TYPE INT');
        $this->addSql('ALTER TABLE t_usulan_hari_libur ALTER nomor_ticket DROP DEFAULT');
        $this->addSql('ALTER TABLE t_usulan_hari_libur ALTER nomor_ticket TYPE INT');
    }
}

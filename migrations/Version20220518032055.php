<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220518032055 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE t_permohonan_cuti ADD kode_nomor VARCHAR(7) DEFAULT NULL');
        $this->addSql('ALTER TABLE t_permohonan_cuti ADD nomor_surat INT DEFAULT NULL');
        $this->addSql('ALTER TABLE t_permohonan_cuti ADD tahun_surat INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE t_permohonan_cuti DROP kode_nomor');
        $this->addSql('ALTER TABLE t_permohonan_cuti DROP nomor_surat');
        $this->addSql('ALTER TABLE t_permohonan_cuti DROP tahun_surat');
    }
}

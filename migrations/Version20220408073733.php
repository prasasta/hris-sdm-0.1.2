<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220408073733 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE t_kantor_uk3tsp (id UUID NOT NULL, usulan_kantor_id UUID NOT NULL, kantor_id TEXT DEFAULT NULL, nama TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_65EF492797733804 ON t_kantor_uk3tsp (usulan_kantor_id)');
        $this->addSql('CREATE INDEX idx_kantor_uk3tsp ON t_kantor_uk3tsp (id)');
        $this->addSql('CREATE INDEX idx_kantor_uk3tsp_ref ON t_kantor_uk3tsp (id)');
        $this->addSql('COMMENT ON COLUMN t_kantor_uk3tsp.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_kantor_uk3tsp.usulan_kantor_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE t_uk3tsp (id UUID NOT NULL, nomor_ticket VARCHAR(50) NOT NULL, pmk TEXT DEFAULT NULL, tanggal DATE NOT NULL, keterangan TEXT DEFAULT NULL, status INT NOT NULL, keterangan_tolak VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_uk3tsp ON t_uk3tsp (id, pmk, tanggal)');
        $this->addSql('CREATE INDEX idx_uk3tsp_ref ON t_uk3tsp (id)');
        $this->addSql('COMMENT ON COLUMN t_uk3tsp.id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE t_kantor_uk3tsp ADD CONSTRAINT FK_65EF492797733804 FOREIGN KEY (usulan_kantor_id) REFERENCES t_uk3tsp (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE t_kantor_uk3tsp DROP CONSTRAINT FK_65EF492797733804');
        $this->addSql('DROP TABLE t_kantor_uk3tsp');
        $this->addSql('DROP TABLE t_uk3tsp');
    }
}

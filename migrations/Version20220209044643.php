<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220209044643 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE t_cuti_pegawai_status (id UUID NOT NULL, nip9 VARCHAR(9) DEFAULT NULL, pegawai_id UUID DEFAULT NULL, hak_cuti INT DEFAULT NULL, kd_status_kepeg VARCHAR(3) NOT NULL, tmt_cpns DATE NOT NULL, cuti_besar_terakhir DATE DEFAULT NULL, count_cuti_besar INT DEFAULT NULL, cltn_terakhir DATE DEFAULT NULL, count_cltn INT DEFAULT NULL, lama_cltn INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_cuti_pegawai_status ON t_cuti_pegawai_status (id, nip9, pegawai_id)');
        $this->addSql('COMMENT ON COLUMN t_cuti_pegawai_status.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_cuti_pegawai_status.pegawai_id IS \'(DC2Type:uuid)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE t_cuti_pegawai_status');
    }
}

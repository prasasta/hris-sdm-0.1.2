<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220708013740 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE t_permohonan_cuti_tambahan ADD kantor VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE t_permohonan_cuti_tambahan ADD unit_organisasi VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE t_permohonan_cuti_tambahan DROP kantor');
        $this->addSql('ALTER TABLE t_permohonan_cuti_tambahan DROP unit_organisasi');
    }
}

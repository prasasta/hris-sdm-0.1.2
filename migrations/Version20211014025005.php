<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211014025005 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE t_hari_libur (id UUID NOT NULL, usulan_id UUID NOT NULL, jenis_id INT NOT NULL, tanggal_awal DATE NOT NULL, tanggal_akhir DATE NOT NULL, keterangan TEXT DEFAULT NULL, scope INT NOT NULL, provinsi_ids TEXT DEFAULT NULL, kota_ids TEXT DEFAULT NULL, kantor_ids TEXT DEFAULT NULL, agama_ids TEXT DEFAULT NULL, active BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8E8801B4592FF605 ON t_hari_libur (usulan_id)');
        $this->addSql('CREATE INDEX IDX_8E8801B4BBD1681E ON t_hari_libur (jenis_id)');
        $this->addSql('CREATE INDEX idx_hari_libur ON t_hari_libur (id, tanggal_awal, tanggal_akhir, jenis_id, scope)');
        $this->addSql('CREATE INDEX idx_hari_libur_ref ON t_hari_libur (id, provinsi_ids, kota_ids, kantor_ids, agama_ids)');
        $this->addSql('COMMENT ON COLUMN t_hari_libur.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_hari_libur.usulan_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_hari_libur.provinsi_ids IS \'(DC2Type:simple_array)\'');
        $this->addSql('COMMENT ON COLUMN t_hari_libur.kota_ids IS \'(DC2Type:simple_array)\'');
        $this->addSql('COMMENT ON COLUMN t_hari_libur.kantor_ids IS \'(DC2Type:simple_array)\'');
        $this->addSql('COMMENT ON COLUMN t_hari_libur.agama_ids IS \'(DC2Type:simple_array)\'');
        $this->addSql('ALTER TABLE t_hari_libur ADD CONSTRAINT FK_8E8801B4592FF605 FOREIGN KEY (usulan_id) REFERENCES t_usulan_hari_libur (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE t_hari_libur ADD CONSTRAINT FK_8E8801B4BBD1681E FOREIGN KEY (jenis_id) REFERENCES t_jenis_libur (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE t_hari_libur');
    }
}

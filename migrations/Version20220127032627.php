<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220127032627 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE t_cuti_bersama (id UUID NOT NULL, nomor_ticket VARCHAR(50) NOT NULL, tanggal_mulai DATE NOT NULL, tanggal_selesai DATE NOT NULL, date_created DATE DEFAULT NULL, created_by UUID DEFAULT NULL, date_approved DATE DEFAULT NULL, approved_by UUID DEFAULT NULL, status INT DEFAULT NULL, keterangan TEXT DEFAULT NULL, reduce INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_cuti_bersama ON t_cuti_bersama (id, nomor_ticket, tanggal_mulai, tanggal_selesai)');
        $this->addSql('COMMENT ON COLUMN t_cuti_bersama.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_cuti_bersama.created_by IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_cuti_bersama.approved_by IS \'(DC2Type:uuid)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE t_cuti_bersama');
    }
}

<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class CommonController extends AbstractController
{
    /**
     * @return JsonResponse
     */
    #[Route('/whoami', name: 'app_whoami', methods: ['POST'])]
    public function whoami(): JsonResponse
    {
        return $this->json($this->getUser());
    }
}

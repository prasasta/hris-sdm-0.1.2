<?php

namespace App\Repository;

use App\Entity\JnsCutiTambahan;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<JnsCutiTambahan>
 *
 * @method JnsCutiTambahan|null find($id, $lockMode = null, $lockVersion = null)
 * @method JnsCutiTambahan|null findOneBy(array $criteria, array $orderBy = null)
 * @method JnsCutiTambahan[]    findAll()
 * @method JnsCutiTambahan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JnsCutiTambahanRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JnsCutiTambahan::class);
    }

    public function add(JnsCutiTambahan $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(JnsCutiTambahan $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}

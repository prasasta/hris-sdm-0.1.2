<?php

namespace App\Repository;

use App\Entity\CutiPegawaiStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CutiPegawaiStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method CutiPegawaiStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method CutiPegawaiStatus[]    findAll()
 * @method CutiPegawaiStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CutiPegawaiStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CutiPegawaiStatus::class);
    }

    // /**
    //  * @return CutiPegawaiStatus[] Returns an array of CutiPegawaiStatus objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CutiPegawaiStatus
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

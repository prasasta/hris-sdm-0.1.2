<?php

namespace App\Repository;

use App\Entity\PermohonanCutiTambahan;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<PermohonanCutiTambahan>
 *
 * @method PermohonanCutiTambahan|null find($id, $lockMode = null, $lockVersion = null)
 * @method PermohonanCutiTambahan|null findOneBy(array $criteria, array $orderBy = null)
 * @method PermohonanCutiTambahan[]    findAll()
 * @method PermohonanCutiTambahan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PermohonanCutiTambahanRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PermohonanCutiTambahan::class);
    }

    public function add(PermohonanCutiTambahan $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(PermohonanCutiTambahan $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return PermohonanCutiTambahan[] Returns an array of PermohonanCutiTambahan objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?PermohonanCutiTambahan
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
    /**
     * @param $pegawaiId
     * @param $limit
     * @param $offset
     * @return mixed
     */
    public function findListPersetujuanByPegawaiId($pegawaiId, $offset, $limit): mixed
    {
        if('' == $limit){
            return $this->createQueryBuilder('p')
                /*->select([
                    'p.id',
                    'p.nomorTicket',
                    'p.pegawaiId',
                    'p.nama',
                    'p.tanggalMulai',
                    'p.tanggalSelesai',
                    'p.lamaCuti',
                    'p.alasan',
                    'p.alamatSementara',
                    'p.atasanLangsung',
                    'p.approvalAtasanLangsung',
                    'p.atasanBerwenang',
                    'p.approvalAtasanBerwenang',
                    'p.status',
                    'p.statusPerawatan',
                    'p.tanggalPerawatanMulai',
                    'p.tanggalPerawatanSelesai',
                    'p.keguguran',
                    'p.ketDokter',
                    'p.tglBerangkatHaji',
                    'p.tglKembaliHaji',
                    'p.tipeCutiHalf'
                ])*/
                ->andWhere('p.atasanLangsung = :pegawaiId AND (p.approvalAtasanLangsung = 0 OR p.approvalAtasanLangsung IS NULL)')
                ->orWhere('p.atasanBerwenang = :pegawaiId and p.approvalAtasanLangsung = 1
                            and (p.approvalAtasanBerwenang IS NULL OR p.approvalAtasanBerwenang = 0)')
                ->andWhere('p.status = 1')
                ->setParameter('pegawaiId', $pegawaiId)
                ->addOrderBy('p.tanggalMulai', 'ASC')
                ->getQuery()
                ->getResult();
        }else{
            return $this->createQueryBuilder('p')
                /*->select([
                    'p.id',
                    'p.nomorTicket',
                    'p.pegawaiId',
                    'p.nama',
                    'p.tanggalMulai',
                    'p.tanggalSelesai',
                    'p.lamaCuti',
                    'p.alasan',
                    'p.alamatSementara',
                    'p.atasanLangsung',
                    'p.approvalAtasanLangsung',
                    'p.atasanBerwenang',
                    'p.approvalAtasanBerwenang',
                    'p.status',
                    'p.statusPerawatan',
                    'p.tanggalPerawatanMulai',
                    'p.tanggalPerawatanSelesai',
                    'p.keguguran',
                    'p.ketDokter',
                    'p.tglBerangkatHaji',
                    'p.tglKembaliHaji',
                    'p.tipeCutiHalf'
                ])*/
                ->andWhere('p.atasanLangsung = :pegawaiId AND (p.approvalAtasanLangsung = 0 OR p.approvalAtasanLangsung IS NULL)')
                ->orWhere('p.atasanBerwenang = :pegawaiId and p.approvalAtasanLangsung = 1
                            and (p.approvalAtasanBerwenang IS NULL OR p.approvalAtasanBerwenang = 0)')
                ->andWhere('p.status = 1')
                ->setParameter('pegawaiId', $pegawaiId)
                ->addOrderBy('p.tanggalMulai', 'ASC')
                ->setFirstResult( $offset )
                ->setMaxResults( $limit )
                ->getQuery()
                ->getResult();
        }
    }

    public function countListPersetujuanByPegawaiId($pegawaiId): mixed
    {
        return count($this->createQueryBuilder('p')
            ->select([
                'p.id'
            ])
            ->andWhere('p.atasanLangsung = :pegawaiId AND (p.approvalAtasanLangsung = 0 OR p.approvalAtasanLangsung IS NULL)')
            ->orWhere('p.atasanBerwenang = :pegawaiId and p.approvalAtasanLangsung = 1
                        and (p.approvalAtasanBerwenang IS NULL OR p.approvalAtasanBerwenang = 0)')
            ->andWhere('p.status = 1')
            ->setParameter('pegawaiId', $pegawaiId)
            ->addOrderBy('p.tanggalMulai', 'ASC')
            ->getQuery()
            ->getResult());
    }

    /**
     * @param $pegawaiId
     * @return mixed
     */
    public function findCutiTambahanDipakai($pegawaiId): mixed
    {
        return $this->createQueryBuilder('p')
            ->select('SUM(p.lamaCutiTambahan) as cutiTambahanDipakai')
            ->andWhere('p.pegawaiId IN(:pegawaiId)')
            ->setParameter('pegawaiId', $pegawaiId)
            ->getQuery()
            ->getResult();
    }
}

<?php

namespace App\Repository;

use App\Entity\HariLibur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HariLibur|null find($id, $lockMode = null, $lockVersion = null)
 * @method HariLibur|null findOneBy(array $criteria, array $orderBy = null)
 * @method HariLibur[]    findAll()
 * @method HariLibur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HariLiburRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HariLibur::class);
    }

    // /**
    //  * @return HariLibur[] Returns an array of HariLibur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HariLibur
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

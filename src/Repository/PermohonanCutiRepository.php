<?php

namespace App\Repository;

use App\Entity\PermohonanCuti;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception;
use Doctrine\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\Types\Integer;

/**
 * @method PermohonanCuti|null find($id, $lockMode = null, $lockVersion = null)
 * @method PermohonanCuti|null findOneBy(array $criteria, array $orderBy = null)
 * @method PermohonanCuti[]    findAll()
 * @method PermohonanCuti[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PermohonanCutiRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PermohonanCuti::class);
    }

    // /**
    //  * @return PermohonanCuti[] Returns an array of PermohonanCuti objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /**
     * @param $pegawaiId
     * @param $limit
     * @param $offset
     * @return mixed
     */
    public function findListPersetujuanByPegawaiId($pegawaiId, $offset, $limit): mixed
    {
        if ('' === $limit || null === $limit) {
            return $this->createQueryBuilder('p')
                ->andWhere('p.atasanLangsung = :pegawaiId AND (p.approvalAtasanLangsung = 0 OR p.approvalAtasanLangsung IS NULL)')
                ->orWhere('p.atasanBerwenang = :pegawaiId and p.approvalAtasanLangsung = 1
                            and (p.approvalAtasanBerwenang IS NULL OR p.approvalAtasanBerwenang = 0)')
                ->andWhere('p.status = 1')
                ->setParameter('pegawaiId', $pegawaiId)
                ->addOrderBy('p.tanggalMulai', 'ASC')
                ->getQuery()
                ->getResult();
        }

        return $this->createQueryBuilder('p')
            ->andWhere('p.atasanLangsung = :pegawaiId AND (p.approvalAtasanLangsung = 0 OR p.approvalAtasanLangsung IS NULL)')
            ->orWhere('p.atasanBerwenang = :pegawaiId and p.approvalAtasanLangsung = 1
                        and (p.approvalAtasanBerwenang IS NULL OR p.approvalAtasanBerwenang = 0)')
            ->andWhere('p.status = 1')
            ->setParameter('pegawaiId', $pegawaiId)
            ->addOrderBy('p.tanggalMulai', 'ASC')
            ->setFirstResult( $offset )
            ->setMaxResults( $limit )
            ->getQuery()
            ->getResult();
    }

    public function countListPersetujuanByPegawaiId($pegawaiId): int
    {
        return count($this->createQueryBuilder('p')
            ->select([
                'p.nomorTicket'
            ])
            ->andWhere('p.atasanLangsung = :pegawaiId AND (p.approvalAtasanLangsung = 0 OR p.approvalAtasanLangsung IS NULL)')
            ->orWhere('p.atasanBerwenang = :pegawaiId and p.approvalAtasanLangsung = 1
                        and (p.approvalAtasanBerwenang IS NULL OR p.approvalAtasanBerwenang = 0)')
            ->andWhere('p.status = 1')
            ->setParameter('pegawaiId', $pegawaiId)
            ->addOrderBy('p.tanggalMulai', 'ASC')
            ->getQuery()
            ->getResult());
    }

    /**
     * @throws Exception
     */
    public function getMaxNomorSurat(string $kodeNomor, string $tahunNomor): mixed
    {
        $entityManager = $this->getEntityManager();
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
                select COALESCE(max(nomor_surat),0)+1 nomor_surat
                from t_permohonan_cuti
                where kode_nomor=:kodeNomor and tahun_surat=:tahunSurat;
            ';
        $stmt       = $conn->prepare($sql);
        $resultSet  = $stmt->executeQuery([
            'kodeNomor'     => $kodeNomor,
            'tahunSurat'    => $tahunNomor
        ]);

        return $resultSet->fetchAllAssociative();
    }
}

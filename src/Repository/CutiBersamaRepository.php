<?php

namespace App\Repository;

use App\Entity\CutiBersama;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CutiBersama|null find($id, $lockMode = null, $lockVersion = null)
 * @method CutiBersama|null findOneBy(array $criteria, array $orderBy = null)
 * @method CutiBersama[]    findAll()
 * @method CutiBersama[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CutiBersamaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CutiBersama::class);
    }

    // /**
    //  * @return CutiBersama[] Returns an array of CutiBersama objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CutiBersama
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

declare(strict_types=1);

namespace App\OpenApi;


use ApiPlatform\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\OpenApi\Model\Operation;
use ApiPlatform\OpenApi\Model\Parameter;
use ApiPlatform\OpenApi\Model\PathItem;
use ApiPlatform\OpenApi\Model\RequestBody;
use ApiPlatform\OpenApi\OpenApi;
use ArrayObject;

final class Uk3tspCustomDecorator implements OpenApiFactoryInterface
{
    public function __construct(
        private OpenApiFactoryInterface $decorated
    ) {}

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = ($this->decorated)($context);
        $schemas = $openApi->getComponents()->getSchemas();

        $schemas['PostUk3tspDataRequest'] = new ArrayObject([
            'type' => 'object',
            'properties' => [
                'uk3tsp' => [
                    'type' => 'array',
                    'example' => ['pmk', 'keterangan', 'status'],
                ],
                'kantor' => [
                    'type' => 'array',
                    'example' => ['kantorId', 'nama'],
                ],
            ],
        ]);

        $schemas['PostUk3tspDataResponse'] = new ArrayObject([
            'type' => 'object',
            'properties' => [
                'uk3tsp' => [
                    'type' => 'array',
                    'example' => [],
                    'readOnly' => true,
                ],
                'kantor' => [
                    'type' => 'array',
                    'example' => [],
                    'readOnly' => true,
                ],
            ],
        ]);
        $permohonanUk3tsp = new PathItem(
            ref: 'permohonan Uk3tsp',
            post: new Operation(
                operationId: 'postUk3tspin2table',
                tags: ['Uk3tsp'],
                responses: [
                    '201' => [
                        'description' => 'Post Permohonan Uk3tsp',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/permohonanUk3tspResponse',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'Post Permohonan Uk3tsp',
                requestBody: new RequestBody(
                    description: 'Post Permohonan Uk3tsp in 2 table',
                    content: new ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/PostUk3tspDataRequest',
                            ],
                        ],
                    ]),
                ),
            ),
        );


        $schemas['PatchUk3tspDataRequest'] = new ArrayObject([
            'type' => 'object',
            'properties' => [
                'uk3tsp' => [
                    'type' => 'array',
                    'example' => ['pmk', 'keterangan', 'status'],
                ],
                'kantor' => [
                    'type' => 'array',
                    'example' => ['kantorId', 'nama'],
                ],
            ],
        ]);

        $schemas['PatchUk3tspDataResponse'] = new ArrayObject([
            'type' => 'object',
            'properties' => [
                'uk3tsp' => [
                    'type' => 'array',
                    'example' => [],
                    'readOnly' => true,
                ],
                'kantor' => [
                    'type' => 'array',
                    'example' => [],
                    'readOnly' => true,
                ],
            ],
        ]);
        $editpermohonanUk3tsp = new PathItem(
            ref: 'edit permohonan Uk3tsp',
            patch: new Operation(
                operationId: 'patchUk3tspin2table',
                tags: ['Uk3tsp'],
                responses: [
                    '201' => [
                        'description' => 'Patch Permohonan Uk3tsp',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/editpermohonanUk3tspResponse',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'Patch Permohonan Uk3tsp',
                parameters: [new Parameter(
                    'id',
                    'path',
                    'Please provide the kantor Id.',
                    true
                )]
            ),
        );

        $activePmk = new PathItem(
            ref: 'Uk3tsp',
            get: new Operation(
                operationId: 'getActivePmk',
                tags: ['Uk3tsp'],
                responses: [
                    '200' => [
                        'description' => 'Get List of Active Pmk',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/GetActivePmkResponse',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'Get list of Active Pmk',
            ),
        );

        $openApi->getPaths()->addPath('/uk3tsps/permohonan_full', $permohonanUk3tsp);
        $openApi->getPaths()->addPath('/uk3tsps/permohonan_full/{id}', $editpermohonanUk3tsp);
        $openApi->getPaths()->addPath('/uk3tsps/active/pmk', $activePmk);


        return $openApi;
    }
}

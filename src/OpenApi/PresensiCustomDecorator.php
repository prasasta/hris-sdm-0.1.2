<?php

declare(strict_types=1);

namespace App\OpenApi;


use ApiPlatform\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\OpenApi\Model\Operation;
use ApiPlatform\OpenApi\Model\Parameter;
use ApiPlatform\OpenApi\Model\PathItem;
use ApiPlatform\OpenApi\Model\RequestBody;
use ApiPlatform\OpenApi\OpenApi;
use ArrayObject;

class PresensiCustomDecorator implements OpenApiFactoryInterface
{

    public function __construct(
        private OpenApiFactoryInterface $decorated
    ) {}

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = ($this->decorated)($context);
        $schemas = $openApi->getComponents()->getSchemas();

        $schemas['GetCutiTahunanDipakaiRequest'] = new ArrayObject([
            'type' => 'object',
            'properties' => [
                'pegawaiId' => [
                    'type' => 'string',
                    'example' => 'fab998cd-e4ca-4b9c-833b-68ebee1a9538',
                ],
                'year' => [
                    'type' => 'string',
                    'example' => '2021',
                ],
                'provinsi' => [
                    'type' => 'string',
                    'example' => '018797a9-4329-404a-8073-f2b7f12c69a3',
                ],
                'kota' => [
                    'type' => 'string',
                    'example' => '5ef7e0b5-5e7d-438b-9fb9-746b177ec692',
                ],
                'agama' => [
                    'type' => 'string',
                    'example' => '0a314d21-3bf3-4d8c-97f9-4c3cab84da33',
                ],
                'tanggalAcuan' => [
                    'type' => 'string',
                    'example' => '20220301',
                ],
            ],
        ]);

        $schemas['GetHariKerjaByTanggalRequest'] = new ArrayObject([
            'type' => 'object',
            'properties' => [
                'tanggalMulai' => [
                    'type' => 'string',
                    'example' => 'yyyy-mm-dd',
                ],
                'tanggalSelesai' => [
                    'type' => 'string',
                    'example' => 'yyyy-mm-dd',
                ],
                'provinsi' => [
                    'type' => 'string',
                    'example' => '0614aadf-dda8-4bc7-a9c1-b369d581be2a',
                ],
                'kota' => [
                    'type' => 'string',
                    'example' => 'd20379b8-9a6d-45f4-be6a-a584e09726b1',
                ],
                'agama' => [
                    'type' => 'string',
                    'example' => 'bbfaa779-fff6-4c67-b633-627a03d11d8f',
                ],
            ],
        ]);

        $schemas['getListPersetujuanRequest'] = new ArrayObject([
            'type' => 'object',
            'properties' => [
                'pegawaiId' => [
                    'type' => 'string',
                    'example' => 'fab998cd-e4ca-4b9c-833b-68ebee1a9538',
                ],
            ],
        ]);

        $schemas['GetCutiTahunanDipakaiResponse'] = new ArrayObject([
            'type' => 'object',
            'properties' => [
                'CutiTahunanDipakai' => [
                    'type' => 'integer',
                    'readOnly' => true,
                ],
            ],

        ]);

        $schemas['getListPersetujuanResponse'] = new ArrayObject([
            'type'          => 'object',
            'properties'    => [
                'list' => [
                    'type'      => 'object',
                    'readOnly'  => true,
                ],
                'count' => [
                    'type' => 'integer',
                    'readOnly' => true,
                ],
            ],
        ]);

        $schemas['GetHariKerjaByTanggalResponse'] = new ArrayObject([
            'type' => 'object',
            'properties' => [
                'lamaHariKerja' => [
                    'type' => 'integer',
                    'readOnly' => true,
                ],
            ],
        ]);

        $schemas['GetHariKerjaCLTByTanggalResponse'] = new ArrayObject([
            'type' => 'object',
            'properties' => [
                'lamaHariKerja' => [
                    'type' => 'integer',
                    'readOnly' => true,
                ],
                "cutiDiambil" => [
                    'type' => 'integer',
                    'readOnly' => true,
                ],
                "cutiLT" => [
                    'type' => 'integer',
                    'readOnly' => true,
                ],
                "tanggalMulai" => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                "tanggalSelesai" => [
                    'type' => 'string',
                    'readOnly' => true,
                ]
            ],
        ]);

        $schemas['getRekapCutiResponse'] = new ArrayObject([
            'type' => 'object',
            'properties' => [
                'hakCuti' => [
                    'type' => 'integer',
                    'readOnly' => true,
                ],
                'kuotaTahunLalu2' => [
                    'type' => 'integer',
                    'readOnly' => true,
                ],
                'kuotaTahunLalu' => [
                    'type' => 'integer',
                    'readOnly' => true,
                ],
                'kuotaTahunIni' => [
                    'type' => 'integer',
                    'readOnly' => true,
                ],
                'n_2' => [
                    'type' => 'integer',
                    'readOnly' => true,
                ],
                'n_1' => [
                    'type' => 'integer',
                    'readOnly' => true,
                ],
                'ct_n2' => [
                    'type' => 'integer',
                    'readOnly' => true,
                ],
                'ct_n1' => [
                    'type' => 'integer',
                    'readOnly' => true,
                ],
                'ct_n' => [
                    'type' => 'integer',
                    'readOnly' => true,
                ],
            ],
        ]);

        $cutiTahunanDipakai = new PathItem(
            ref: 'Cuti Pegawai',
            post: new Operation(
                operationId: 'postPegawaiIdnYearToGetCutiDipakai',
                tags: ['RekapCuti'],
                responses: [
                    '200' => [
                        'description' => 'Get Sum of Cuti Diambil',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/getRekapCutiResponse',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'Get Sum of Cuti Terpakai From Id Pegawai',
                requestBody: new RequestBody(
                    description: 'Get Sum Cuti Dipakai',
                    content: new ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/GetCutiTahunanDipakaiRequest',
                            ],
                        ],
                    ]),
                ),
            ),
        );

        $listPersetujuan = new PathItem(
            ref: 'Permohonan Cuti',
            post: new Operation(
                operationId: 'postPegawaiIdToGetListPersetujuan',
                tags: ['PermohonanCuti'],
                responses: [
                    '200' => [
                        'description' => 'Get List of Persetujuan Cuti',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/getListPersetujuanResponse',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'Get List of Persetujuan Cuti',
                requestBody: new RequestBody(
                    description: 'Get List Persetujuan Cuti',
                    content: new ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/getListPersetujuanRequest',
                            ],
                        ],
                    ]),
                ),
            ),
        );

        $countHariKerja = new PathItem(
            ref: 'Cuti Pegawai',
            post: new Operation(
                operationId: 'postTanggalToGetCountHariKerja',
                tags: ['RekapCuti'],
                responses: [
                    '200' => [
                        'description' => 'Get Count Working Days',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/GetHariKerjaByTanggalResponse',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'Get Count Working Days Based On Date Range',
                requestBody: new RequestBody(
                    description: 'Get Count Working Days',
                    content: new ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/GetHariKerjaByTanggalRequest',
                            ],
                        ],
                    ]),
                ),
            ),
        );

        $countHariKerjaCLT = new PathItem(
            ref: 'Cuti Pegawai',
            post: new Operation(
                operationId: 'postTanggalToGetCountHariKerjaCLT',
                tags: ['RekapCuti'],
                responses: [
                    '200' => [
                        'description' => 'Get Count Working Days',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/GetHariKerjaCLTByTanggalResponse',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'Get Count Working Days Based On Date Range',
                requestBody: new RequestBody(
                    description: 'Get Count Working Days',
                    content: new ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/GetHariKerjaByTanggalRequest',
                            ],
                        ],
                    ]),
                ),
            ),
        );

        $rekapCuti = new PathItem(
            ref: 'Cuti Pegawai',
            post: new Operation(
                operationId: 'postPegawaiIdnYearToGetRekapCuti',
                tags: ['RekapCuti'],
                responses: [
                    '200' => [
                        'description' => 'Get Summary Cuti',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/getRekapCutiResponse',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'Get Summary Cuti From Id Pegawai',
                requestBody: new RequestBody(
                    description: 'Get Summary Cuti',
                    content: new ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/GetCutiTahunanDipakaiRequest',
                            ],
                        ],
                    ]),
                ),
            ),
        );

        $deleteRiwayatCuti = new PathItem(
            ref: 'Cuti Pegawai',
            delete: new Operation(
                operationId: 'delete_by_permohonan_id',
                tags: ['CutiPegawai'],
                responses: [
                    '204' => [
                        'description' => 'Delete Riwayat Cuti',
                    ],
                ],
                summary: 'Delete cuti pegawais record related to permohonan id',
                parameters: [new Parameter(
                    'permohonanId',
                    'path',
                    'Please provide the permohonan ID with UUID format',
                    true
                )],
            ),
        );

        $openApi->getPaths()->addPath('/rekap_cutis/cuti_diambil', $cutiTahunanDipakai);
        $openApi->getPaths()->addPath('/rekap_cutis/sisa_cuti', $rekapCuti);
        $openApi->getPaths()->addPath('/rekap_cutis/lama_cuti', $countHariKerja);
        $openApi->getPaths()->addPath('/rekap_cutis/lama_cuti_clt', $countHariKerjaCLT);
        $openApi->getPaths()->addPath('/cuti_pegawais/delete_by_permohonan/{permohonanId}', $deleteRiwayatCuti);
        $openApi->getPaths()->addPath('/permohonan_cutis/get_list_persetujuan_cuti', $listPersetujuan);

        return $openApi;
    }
}

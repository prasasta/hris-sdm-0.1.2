<?php

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\KantorUk3tspRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV6;

#[ApiResource(
    security: 'is_granted(\'ROLE_USER\')'
)]
#[ORM\Entity(
    repositoryClass: KantorUk3tspRepository::class
)]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(
    name: 't_kantor_uk3tsp'
)]
#[ORM\Index(
    columns: [
        'id'
    ],
    name: 'idx_kantor_uk3tsp'
)]
#[ORM\Index(
    columns: [
        'id'
    ],
    name: 'idx_kantor_uk3tsp_ref'
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'id' => 'exact',
        'kantorId' => 'ipartial',
        'nama' => 'ipartial'
    ]
)]
class KantorUk3tsp
{
    #[ORM\Id]
    #[ORM\Column(
        type: 'uuid',
        unique: true
    )]
    #[Groups(
        groups: [
            'uk3tsp:read'
        ]
    )]
    private UuidV6 $id;

    #[ORM\Column(
        type: 'text',
        nullable: true
    )]

    #[Groups(
        groups: [
            'uk3tsp:read'
        ]
    )]
    private ?string $kantorId;

    #[ORM\Column(
        type: 'text',
        nullable: true
    )]

    #[Groups(
        groups: [
            'uk3tsp:read'
        ]
    )]
    private ?string $nama;

    #[ORM\ManyToOne(targetEntity: Uk3tsp::class, inversedBy: 'kantor')]
    #[ORM\JoinColumn(nullable: false)]

    private ?Uk3tsp $usulanKantor;

    public function __construct()
    {
        $this->id = Uuid::v6();
    }

    public function getId(): UuidV6
    {
        return $this->id;
    }

    public function getKantorId(): ?string
    {
        return $this->kantorId;
    }

    public function setKantorId(?string $kantorId): self
    {
        $this->kantorId = $kantorId;

        return $this;
    }

    public function getNama(): ?string
    {
        return $this->nama;
    }

    public function setNama(?string $nama): self
    {
        $this->nama = $nama;

        return $this;
    }

    public function getUsulanKantor(): ?Uk3tsp
    {
        return $this->usulanKantor;
    }

    public function setUsulanKantor(?Uk3tsp $usulanKantor): self
    {
        $this->usulanKantor = $usulanKantor;

        return $this;
    }
}

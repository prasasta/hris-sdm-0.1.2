<?php

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\JenisLiburRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    security: 'is_granted(\'ROLE_USER\')'
)]
#[ORM\Entity(
    repositoryClass: JenisLiburRepository::class
)]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(
    name: 't_jenis_libur'
)]
#[ORM\Index(
    columns: [
        'id',
        'nama',
        'tanggal_aktif',
        'tanggal_non_aktif'
    ],
    name: 'idx_jenis_libur'
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'nama' => 'ipartial'
    ]
)]
#[ApiFilter(
    filterClass: DateFilter::class,
    properties: [
        'tanggalAktif',
        'tanggalNonAktif'
    ]
)]
class JenisLibur
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(
        type: 'integer'
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read'
        ]
    )]
    private ?int $id;

    #[ORM\Column(
        type: 'string',
        length: 255
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read'
        ]
    )]
    private ?string $nama;

    #[ORM\Column(
        type: 'date'
    )]
    private ?DateTimeInterface $tanggalAktif;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    private ?DateTimeInterface $tanggalNonAktif;

    #[ORM\OneToMany(
        mappedBy: 'jenis',
        targetEntity: HariLibur::class
    )]
    private Collection $hariLiburs;

    public function __construct()
    {
        $this->hariLiburs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNama(): ?string
    {
        return $this->nama;
    }

    public function setNama(string $nama): self
    {
        $this->nama = $nama;

        return $this;
    }

    public function getTanggalAktif(): ?DateTimeInterface
    {
        return $this->tanggalAktif;
    }

    public function setTanggalAktif(DateTimeInterface $tanggalAktif): self
    {
        $this->tanggalAktif = $tanggalAktif;

        return $this;
    }

    #[ORM\PrePersist]
    public function setTanggalAktifValue(): void
    {
        $this->tanggalAktif = new DateTime('now');
    }

    public function getTanggalNonAktif(): ?DateTimeInterface
    {
        return $this->tanggalNonAktif;
    }

    public function setTanggalNonAktif(?DateTimeInterface $tanggalNonAktif): self
    {
        $this->tanggalNonAktif = $tanggalNonAktif;

        return $this;
    }

    public function isActive(): bool
    {
        $today = new DateTime('now');
        return ($today >= $this->tanggalAktif
            && (null === $this->tanggalNonAktif || $today <= $this->tanggalNonAktif));
    }

    /**
     * @return Collection|HariLibur[]
     */
    public function getHariLiburs(): Collection
    {
        return $this->hariLiburs;
    }

    public function addHariLibur(HariLibur $hariLibur): self
    {
        if (!$this->hariLiburs->contains($hariLibur)) {
            $this->hariLiburs[] = $hariLibur;
            $hariLibur->setJenis($this);
        }

        return $this;
    }

    public function removeHariLibur(HariLibur $hariLibur): self
    {
        if ($this->hariLiburs->removeElement($hariLibur)) {
            // set the owning side to null (unless already changed)
            if ($hariLibur->getJenis() === $this) {
                $hariLibur->setJenis(null);
            }
        }

        return $this;
    }
}

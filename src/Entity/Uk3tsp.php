<?php

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Helper\AppHelper;
use App\Repository\Uk3tspRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV6;

#[ApiResource(
    security: 'is_granted(\'ROLE_USER\')',
    denormalizationContext: [
        'groups' => [
            'uk3tsp:write'
        ],
        'swagger_definition_name' => 'write'
    ],
    normalizationContext: [
        'groups' => [
            'uk3tsp:read'
        ],
        'swagger_definition_name' => 'read'
    ]
)]
#[ORM\Entity(
    repositoryClass: Uk3tspRepository::class
)]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(
    name: 't_uk3tsp'
)]
#[ORM\Index(
    columns: [
        'id',
        'pmk',
        'tanggal'
    ],
    name: 'idx_uk3tsp'
)]
#[ORM\Index(
    columns: [
        'id'
    ], name: 'idx_uk3tsp_ref'
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'usulan.id' => 'exact',
        'pmk' => 'ipartial',
        'nomorTicket' => 'ipartial',
        'keterangan' => 'ipartial',
        'kantor' => 'ipartial',
        'usulan.pmk' => 'ipartial'
    ]
)]
#[ApiFilter(
    filterClass: DateFilter::class,
    properties: [
        'tanggal'
    ]
)]
#[ApiFilter(
    filterClass: NumericFilter::class,
    properties: [
        'status'
    ]
)]
class Uk3tsp
{
    #[ORM\Id]
    #[ORM\Column(
        type: 'uuid',
        unique: true
    )]
    #[Groups(
        groups: [
            'uk3tsp:read',
            'uk3tsp:write'
        ]
    )]

    private UuidV6 $id;

    #[ORM\Column(
        type: 'string', length: 50
    )]
    #[Groups(
        groups: [
            'uk3tsp:read',
            'uk3tsp:write'
        ]
    )]
    private ?string $nomorTicket;

    #[ORM\Column(
        type: 'text',
        nullable: true
    )]
    #[Groups(
        groups: [
            'uk3tsp:read',
            'uk3tsp:write'
        ]
    )]
    private ?string $pmk;

    #[ORM\Column(
        type: 'date'
    )]
    #[Groups(
        groups: [
            'uk3tsp:read',
            'uk3tsp:write'
        ]
    )]
    private ?DateTimeInterface $tanggal;


    #[ORM\Column(
        type: 'text',
        nullable: true
    )]
    #[Groups(
        groups: [
            'uk3tsp:read',
            'uk3tsp:write'
        ]
    )]
    private ?string $keterangan;

    #[ORM\Column(
        type: 'integer'
    )]
    #[Groups(
        groups: [
            'uk3tsp:read',
            'uk3tsp:write'
        ]
    )]
    private ?int $status;

    #[ORM\OneToMany(
        mappedBy: 'usulanKantor',
        targetEntity: KantorUk3tsp::class,
        orphanRemoval: true
    )]
    #[Groups(
        groups: [
            'uk3tsp:read',
            'uk3tsp:write'
        ]
    )]
    private Collection $kantor;

    #[ORM\Column(
        type: 'string',
        length: 255,
        nullable: true
    )]
    #[Groups(
        groups: [
            'uk3tsp:read',
            'uk3tsp:write'
        ]
    )]
    private ?string $keteranganTolak;

    public function __construct()
    {
        $this->id = Uuid::v6();
        $this->kantor = new ArrayCollection();
    }

    public function getId(): UuidV6
    {
        return $this->id;
    }

    public function getNomorTicket(): ?string
    {
        return $this->nomorTicket;
    }

    public function setNomorTicket(string $nomorTicket): self
    {
        $this->nomorTicket = $nomorTicket;

        return $this;
    }

    /**
     * @throws Exception
     */
    #[ORM\PrePersist]
    public function setNomorTicketValue(): void
    {
        $this->nomorTicket = 'USP-' . AppHelper::RandomString(3) . round(microtime(true));
    }

    public function getPmk(): ?string
    {
        return $this->pmk;
    }

    public function setPmk(?string $pmk): self
    {
        $this->pmk = $pmk;

        return $this;
    }

    public function getTanggal(): ?DateTimeInterface
    {
        return $this->tanggal;
    }

    public function setTanggal(DateTimeInterface $tanggal): self
    {
        $this->tanggal = $tanggal;

        return $this;
    }

    public function getKeterangan(): ?string
    {
        return $this->keterangan;
    }

    public function setKeterangan(?string $keterangan): self
    {
        $this->keterangan = $keterangan;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getKantor(): Collection
    {
        return $this->kantor;
    }

    public function addKantor(KantorUk3tsp $kantor): self
    {
        if (!$this->kantor->contains($kantor)) {
            $this->kantor[] = $kantor;
            $kantor->setUsulanKantor($this);
        }

        return $this;
    }

    public function removeKantor(KantorUk3tsp $kantor): self
    {
        if ($this->kantor->removeElement($kantor)) {
            // set the owning side to null (unless already changed)
            if ($kantor->getUsulanKantor() === $this) {
                $kantor->setUsulanKantor(null);
            }
        }

        return $this;
    }

    public function getKeteranganTolak(): ?string
    {
        return $this->keteranganTolak;
    }

    public function setKeteranganTolak(?string $keteranganTolak): self
    {
        $this->keteranganTolak = $keteranganTolak;

        return $this;
    }
}

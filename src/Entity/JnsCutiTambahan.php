<?php

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\JnsCutiTambahanRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    security: 'is_granted(\'ROLE_USER\')'
)]
#[ORM\Entity(
    repositoryClass: JnsCutiTambahanRepository::class
)]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(
    name: 't_jns_cuti_tambahan'
)]
#[ORM\Index(
    columns: [
        'id',
        'nama',
        'jenis'
    ],
    name: 'idx_jns_cuti_tambahan'
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'nama' => 'ipartial'
    ]
)]
#[ApiFilter(
    NumericFilter::class,
    properties: [
        'jenis'
    ]
)]
class JnsCutiTambahan
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private ?int $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private ?string $nama;

    #[ORM\Column(type: 'integer')]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private ?int $jenis;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNama(): ?string
    {
        return $this->nama;
    }

    public function setNama(string $nama): self
    {
        $this->nama = $nama;

        return $this;
    }

    public function getJenis(): ?int
    {
        return $this->jenis;
    }

    public function setJenis(int $jenis): self
    {
        $this->jenis = $jenis;

        return $this;
    }
}

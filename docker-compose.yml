version: "3.4"

services:
  ###> doctrine/doctrine-bundle ###
  database:
    image: postgres:${POSTGRES_VERSION:-15}-alpine
    environment:
      POSTGRES_DB: ${POSTGRES_DB:-db_name}
      # You should definitely change the password in production
      POSTGRES_PASSWORD: ${POSTGRES_PASSWORD:-db_pass}
      POSTGRES_USER: ${POSTGRES_USER:-db_user}
    ports:
      - "5432:5432"
    restart: unless-stopped
    networks:
      - djp
    volumes:
      - db_data:/var/lib/postgresql/data:rw
      # You may use a bind-mounted host directory instead, so that it is harder to accidentally remove the volume and lose all your data!
      # - ./docker/db/data:/var/lib/postgresql/data:rw
###< doctrine/doctrine-bundle ###

  redis:
    image: redis:latest
    ports:
      - "6379:6379"
    restart: unless-stopped
    networks:
      - djp

  php:
    build:
      context: .
      target: app_php
      args:
        SYMFONY_VERSION: ${SYMFONY_VERSION:-}
        STABILITY: ${STABILITY:-stable}
    restart: unless-stopped
    ports:
      - "9000:9000"
    healthcheck:
      interval: 10s
      timeout: 3s
      retries: 3
      start_period: 30s
    depends_on:
      - redis
      - database
    links:
      - database
      - redis
    networks:
      - djp
    environment:
      # Run "composer require symfony/orm-pack" to install and configure Doctrine ORM
      DATABASE_URL: postgresql://${POSTGRES_USER:-db_user}:${POSTGRES_PASSWORD:-db_pass}@database:5432/${POSTGRES_DB:-db_name}?serverVersion=${POSTGRES_VERSION:-15}
      # Run "composer require symfony/mercure-bundle" to install and configure the Mercure integration
      MERCURE_URL: ${CADDY_MERCURE_URL:-http://caddy/.well-known/mercure}
      MERCURE_PUBLIC_URL: https://${SERVER_NAME:-localhost}/.well-known/mercure
      MERCURE_JWT_SECRET: ${CADDY_MERCURE_JWT_SECRET:-!ChangeThisMercureHubJWTSecretKey!}
      REDIS_URL: redis://redis:6379

  caddy:
    build:
      context: .
      target: app_caddy
    depends_on:
      - php
    environment:
      SERVER_NAME: ${SERVER_NAME:-localhost, caddy:80}
      MERCURE_PUBLISHER_JWT_KEY: ${CADDY_MERCURE_JWT_SECRET:-!ChangeThisMercureHubJWTSecretKey!}
      MERCURE_SUBSCRIBER_JWT_KEY: ${CADDY_MERCURE_JWT_SECRET:-!ChangeThisMercureHubJWTSecretKey!}
      PWA_UPSTREAM: ${CADDY_PWA_UPSTREAM:-localhost}
      BACKEND_FASTCGI: ${BACKEND_FASTCGI_CONTAINER_NAME:-php}:${BACKEND_FASTCGI_CONTAINER_PORT:-9000}
    restart: unless-stopped
    networks:
      - djp
    volumes:
      - caddy_data:/data
      - caddy_config:/config
    ports:
      # HTTP
      - target: 80
        published: ${HTTP_PORT:-80}
        protocol: tcp
      # HTTPS
      - target: 443
        published: ${HTTPS_PORT:-443}
        protocol: tcp
      # HTTP/3
      - target: 443
        published: ${HTTP3_PORT:-443}
        protocol: udp

  nginx:
    build:
      context: .
      target: app_nginx
    depends_on:
      - php
    environment:
      SERVER_NAME: ${SERVER_NAME:-localhost}
      MERCURE_PUBLISHER_JWT_KEY: ${CADDY_MERCURE_JWT_SECRET:-!ChangeThisMercureHubJWTSecretKey!}
      MERCURE_SUBSCRIBER_JWT_KEY: ${CADDY_MERCURE_JWT_SECRET:-!ChangeThisMercureHubJWTSecretKey!}
      PWA_UPSTREAM: ${CADDY_PWA_UPSTREAM:-localhost}
      BACKEND_FASTCGI: ${BACKEND_FASTCGI_CONTAINER_NAME:-php}:${BACKEND_FASTCGI_CONTAINER_PORT:-9000}
    restart: unless-stopped
    links:
      - php
    networks:
      - djp
    # volumes:
    #      - nginx_data:/data
    #      - nginx_config:/config
    ports:
      # HTTP
      - target: 80
        published: ${HTTP_PORT:-80}
        protocol: tcp
      # HTTPS
      - target: 443
        published: ${HTTPS_PORT:-443}
        protocol: tcp
      # HTTP/3
      - target: 443
        published: ${HTTP3_PORT:-443}
        protocol: udp

# Mercure is installed as a Caddy module, prevent the Flex recipe from installing another service
###> symfony/mercure-bundle ###
###< symfony/mercure-bundle ###

volumes:
  php_socket:
  ###> doctrine/doctrine-bundle ###
  db_data:
###< doctrine/doctrine-bundle ###
  caddy_data:
  caddy_config:
#  nginx_data:
#  nginx_conig:

networks:
  djp:
    driver: bridge

###> symfony/mercure-bundle ###
###< symfony/mercure-bundle ###
